%% Discretization size and problem setting

nx=100; % discretization size x axis

v_=[2 1]*0;
Wtype=1;

tol=1e-6;
m=100;
maxit=1;
all_res_it=[];
all_res_flag=[];
for delta_=[1e2 1 1e-2 1e-4 1e-6]
for l=[0 1 2]
for r=[1e-2 1e0 1e2 1e4 1e6 1e8]

delta=delta_;
v=v_;
%%
[mat,rhs,x,freeDOF,basis1,basis2,tri_grid]=conv_diff(nx,delta,v);

%% 
A=TEFEM.sol.block_mult_op(mat,freeDOF);
B_l=TEFEM.sol.triangle_precond(mat,freeDOF,l,r,Wtype);
[rhs_vec,x_vec,freeDOF_vec,idx] = TEFEM.sol.build_vectors_iter_sol(mat,rhs,x,freeDOF);

x_iter=x_vec(freeDOF_vec);
rhs_iter=rhs_vec(freeDOF_vec);

%% Solution
%[x_iter,nit,resvec] = TEFEM.sol.fgmres(A,rhs_iter,m,tol,maxit,B_l,x_iter);
[x_iter,flag,~,nit,resvec]=gmres(A,rhs_iter,m,tol,1,B_l);
%res=TEFEM.sol.direct_block(mat,rhs,x,freeDOF);
all_res_it=[all_res_it nit(2)]
all_res_flag=[all_res_flag flag]
end
end
end

all_res=reshape(all_res_it,6,3,5);
all_flag=reshape(all_res_flag,6,3,5);

%% fill with dirichlet conditions
x_vec(freeDOF_vec)=x_iter;

%% extract solution fields
res=cell(2,1);
for i=1:2
    res{i}=x_vec(idx(i)+1:idx(i+1));
end

%% plotting
basis1.plot(res{1},tri_grid);
basis2.plot(res{2},tri_grid);