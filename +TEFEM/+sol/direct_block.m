function [res,A] = direct_block(mat,rhs,x,free_node)
%DIRECT_BLOCK Summary of this function goes here
%   Detailed explanation goes here
n=size(mat,1);
sizes=zeros(n);
for i=1:n
    for j=1:n
        sizes(i,j)=size(mat{i,j},1);
    end
end
sizes=max(sizes,[],2);
for i=1:n
    if size(rhs{i},1)==0
        rhs{i}=zeros(sizes(i),1);
    end
    if size(x{i},1)==0
        x{i}=zeros(sizes(i),1);
    end
    if size(free_node{i},1)==0
        free_node{i}=true(sizes(i),1);
    end
    for j=1:n
        if size(mat{i,j},1)==0
            mat{i,j}=sparse(sizes(i),sizes(j));
        end
    end
end

A=cell2mat(mat);
b=cell2mat(rhs);
u=cell2mat(x);
fd=cell2mat(free_node);
b=b-A*u;
u(fd)=A(fd,fd)\b(fd);
res=cell(n,1);
idx=[0; cumsum(sizes)];
for i=1:n
    res{i}=u(idx(i)+1:idx(i+1));
end
end

