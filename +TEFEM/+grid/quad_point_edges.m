function [qd] = quad_point_edges(tg, edges, poly_degree)
%QUAD_PREPARATION Summary of this function goes here
%   Detailed explanation goes here
[x, w] = TEFEM.quad.quad_edge(poly_degree);
m=size(x,1);
n=size(edges,1);

qd.elem_idx=tg.edge2elem(edges,1);
for i=1:3
    for j=1:2
        qd.P{i,j}=repmat(tg.node(tg.elem(qd.elem_idx,i),j),1,m);
        qd.n{i,j}=repmat(tg.edge_global_normal(tg.elem2edge(qd.elem_idx,i),j),1,m);
    end
    qd.sigma{i}=repmat(tg.elem_outer_normal_orientation(qd.elem_idx,i),1,m);
    qd.se{i}=repmat(tg.edge_lengths(tg.elem2edge(qd.elem_idx,i)),1,m);
end
for i=1:2
    qd.outer_normal{i}=repmat(tg.edge_global_normal(edges,i).*tg.edge2elem_orientation(edges,1),1,m);
end
xo=repmat(((x+1)/2)',n,1);
P=cell(2,2);
for i=1:2
    for j=1:2
        P{i,j}=repmat(tg.node(tg.edge(edges,i),j),1,m);
    end
end
for i=1:2
    qd.X{i}=P{2,i}.*xo+(1-xo).*P{1,i};
end
qd.elem_sizes=repmat(tg.elem_sizes(qd.elem_idx),1,m);
qd.sizes=tg.edge_lengths(edges);
qd.weights=w/2;
end