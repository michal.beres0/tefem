function [out_indexes] = select_boundary_edges_points(tri_grid,bound_idx,bound_selection)
%QUAD_POINT_VALUE_BOUNDARY Summary of this function goes here
% (1-bottom,2-right,3-top,4-left))
n=size(bound_selection,1);
Lx=tri_grid.Lx;
Ly=tri_grid.Ly;
idx_edges_all=cell(n,1);
for i=1:n
    boundary_site=bound_selection{i,1};
    boundary_part=bound_selection{i,2};
    idx_edges=bound_idx.bdN{boundary_site};
    e_mids=tri_grid.edge_center(idx_edges,:);
    
    if boundary_site==1
        scaled_pos = e_mids(:,1)/Lx;
    end
    if boundary_site==2
        scaled_pos = e_mids(:,2)/Ly;
    end
    if boundary_site==3
        scaled_pos = e_mids(:,1)/Lx;
    end
    if boundary_site==4
        scaled_pos = e_mids(:,2)/Ly;
    end
    id_selected=scaled_pos<=boundary_part(2) & scaled_pos>=boundary_part(1);
    idx_edges_all{i}=idx_edges(id_selected)';
end
out_indexes.idx_e=unique(cell2mat(idx_edges_all));
idx_nodes_all=tri_grid.edge(out_indexes.idx_e,:);
out_indexes.idx_n=unique(idx_nodes_all(:));
end

