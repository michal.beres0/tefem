function [basis] = RT0()
basis.val=@(tg,qd)RT0_val(tg,qd);
basis.dir=@(tg,dir_idx,dir_fun)RT0_dir(tg,dir_idx,dir_fun);
basis.plot=@(u,tri_grid)RT0_plot(u,tri_grid);
end

function [bf] = RT0_val(tg,qd)
%P1 Summary of this function goes here
%   Detailed explanation goes here
bf=cell(3,1);
for i=1:3
    a=qd.sigma{i}.*qd.se{i}./2./qd.elem_sizes;
    for k=1:2
        v.val{k}=a.*(qd.X{k}-qd.P{i,k});
        for l=1:2
            v.grad{k,l}=a;
        end
    end
    v.loc=tg.elem2edge(qd.elem_idx,i);
    v.size=size(tg.edge,1);
    bf{i}=v;
end
end

function [x,freeDOF] = RT0_dir(tg,dir_idx,dir_fun)
%P1_DIR Summary of this function goes here
%   Detailed explanation goes here
idx_e=dir_idx.idx_e;
orientation=tg.edge2elem_orientation(idx_e,1);
x=tg.edge_center(idx_e,1);
y=tg.edge_center(idx_e,2);
val=dir_fun(x,y);
freeDOF=true(size(tg.edge,1),1);
freeDOF(idx_e)=0;
x=zeros(size(tg.edge,1),1);
x(idx_e)=-orientation.*val;
end

function [f] = RT0_plot(u,tg)
points=[0 0;1 0; 0 1];
m=size(points,1);
n=size(tg.elem,1);
qd.elem_idx=(1:n)';
for i=1:3
    for j=1:2
        qd.P{i,j}=repmat(tg.node(tg.elem(qd.elem_idx,i),j),1,m);
        qd.n{i,j}=repmat(tg.edge_global_normal(tg.elem2edge(qd.elem_idx,i),j),1,m);
    end
    qd.sigma{i}=repmat(tg.elem_outer_normal_orientation(qd.elem_idx,i),1,m);
    qd.se{i}=repmat(tg.edge_lengths(tg.elem2edge(qd.elem_idx,i)),1,m);
end

xo{1}=repmat(points(:,1)',n,1);
xo{2}=repmat(points(:,2)',n,1);

qd.X{1}=(qd.P{2,1}-qd.P{1,1}).*xo{1}+(qd.P{3,1}-qd.P{1,1}).*xo{2}+qd.P{1,1};
qd.X{2}=(qd.P{2,2}-qd.P{1,2}).*xo{1}+(qd.P{3,2}-qd.P{1,2}).*xo{2}+qd.P{1,2};

qd.elem_sizes=repmat(tg.elem_sizes(qd.elem_idx),1,m);
qd.sizes=tg.elem_sizes(qd.elem_idx);

val=cell(2,1);
val{1}=qd.X{1}*0;
val{2}=qd.X{1}*0;
for i=1:3
    a=qd.sigma{i}.*qd.se{i}./2./qd.elem_sizes;
    for k=1:2
        val{k}=val{k}+(a.*(qd.X{k}-qd.P{i,k})).*u(tg.elem2edge(qd.elem_idx,i));
    end
end

f=figure;
subplot(1,2,1);
xx=qd.X{1}';
yy=qd.X{2}';
tmp=val{1}';
P=[xx(:) yy(:)];
T=reshape(1:size(P,1),[3 size(P,1)/3]);

h=trisurf(T',P(:,1),P(:,2),tmp(:));
h.EdgeColor = 'none';
colormap jet(1000)
axis equal
view(0,90)
colorbar
title('$RT0$ elem. in $x$ dir','Interpreter','latex')

subplot(1,2,2);
xx=qd.X{1}';
yy=qd.X{2}';
tmp=val{2}';
P=[xx(:) yy(:)];
T=reshape(1:size(P,1),[3 size(P,1)/3]);

h=trisurf(T',P(:,1),P(:,2),tmp(:));
h.EdgeColor = 'none';
colormap jet(1000)
axis equal
view(0,90)
colorbar
title('$RT0$ elem. in $y$ dir','Interpreter','latex')
end