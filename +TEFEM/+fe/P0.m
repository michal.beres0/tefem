function [basis] = P0()
%P1 Summary of this function goes here
%   Detailed explanation goes here
basis.val=@(tg,qd)P0_val(tg,qd);
%basis.dir=
basis.plot=@(u,tri_grid)P0_plot(u,tri_grid);
end

function [bf] = P0_val(tg,qd)
%P1 Summary of this function goes here
%   Detailed explanation goes here
bf=cell(1,1);
v.val=qd.X{1}*0+1;
v.loc=qd.elem_idx;
v.size=size(tg.elem,1);
bf{1}=v;
end

function [f] = P0_plot(u,tri_grid)
f=figure;
p=tri_grid.node';
t=tri_grid.elem';
x=p(1,:);
y=p(2,:);
P=[x(t(:));y(t(:))];
T=reshape(1:size(P,2),[3 size(P,2)/3]);
tmp=[u';u';u'];
h=trisurf(T',P(1,:),P(2,:),tmp(:));
h.EdgeColor = 'none';
colormap jet(1000)
axis equal
view(0,90)
colorbar
title('$P0$ elem.','Interpreter','latex')
end