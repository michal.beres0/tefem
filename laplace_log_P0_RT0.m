%% Discretization size and problem setting
nx=200; % discretization size x axis
ny=nx; % y axis
Lx=1;   % size of the domain x axis
Ly=1;   % y axis
smoothing_boundary = false; % deforms grid to be smoother around the boundary
quad_poly_degree=10;

% (1-bottom,2-right,3-top,4-left))
bound_dir={1, [0 1]; 3, [0 1]};
bound_neu={2, [0 1]; 4, [0 1]};

m_weak=@(v1,v2,data)TEFEM.op.dot(v1.val,v2.val);
b_weak=@(v1,v2,data)-TEFEM.op.trace(v2.grad).*v1.val;
c_weak=@(v1,v2,data)-TEFEM.op.dot(data.k_grad,v2.val).*v1.val;
f_weak=@(v1,data)-data.f.*v1.val;
u0_weak=@(v1,data)-data.u0.*TEFEM.op.dot(v1.val,data.n);

dd_f.k=@(x,y)exp(2*sin(10*pi*x).*cos(10*pi*y));
dd_f.k_grad=@(x,y){10*pi*2*cos(10*pi*x).*cos(10*pi*y),-10*pi*2*sin(10*pi*x).*sin(10*pi*y)};
dd_f.f=@(x,y)x*0+1;
dn_f.u0=@(x,y)x;
g=@(x,y)y*0+1;

basis1=TEFEM.fe.RT0();
basis2=TEFEM.fe.P0();

%% triangulation 
[ tri_grid, bound_idx ] = TEFEM.grid.rect_mesh( Ly, Lx, ny, nx, smoothing_boundary);

%% domain integration
[qd] = TEFEM.grid.quad_point_triangles(tri_grid, quad_poly_degree);
[bf1] = basis1.val(tri_grid,qd);
[bf2] = basis2.val(tri_grid,qd);
for i=fieldnames(dd_f)'
    data.(i{1})=dd_f.(i{1})(qd.X{1},qd.X{2});
end

%% resolving boundary conditions
neu_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_neu);
dir_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_dir);

%% boundary integration
[qd_neu] = TEFEM.grid.quad_point_edges(tri_grid, neu_idx.idx_e, quad_poly_degree);
[bf1_neu] = basis1.val(tri_grid,qd_neu);
[bf2_neu] = basis2.val(tri_grid,qd_neu);
for i=fieldnames(dn_f)'
    data.(i{1})=dn_f.(i{1})(qd_neu.X{1},qd_neu.X{2});
end
data.n=qd_neu.outer_normal;

%% boundary values
[x_1,freeDOF_1] = basis1.dir(tri_grid,dir_idx,g);

%% assembling matrices and vectors
[res_M] = TEFEM.assemble.bilinear(m_weak,bf1,bf1,data,qd);
[res_B] = TEFEM.assemble.bilinear(b_weak,bf2,bf1,data,qd);
[res_C] = TEFEM.assemble.bilinear(c_weak,bf2,bf1,data,qd);
[res_f] = TEFEM.assemble.linear(f_weak,bf2,data,qd);
[res_g] = TEFEM.assemble.linear(u0_weak,bf1_neu,data,qd_neu);

%% solution
freeDOF={freeDOF_1;[]};

mat1={res_M,res_B';res_B+res_C,[]};
rhs1={res_g;res_f*0};
x1={x_1*0;[]};
res1=TEFEM.sol.direct_block(mat1,rhs1,x1,freeDOF);

mat2={res_M,res_B'+res_C';res_B,[]};
rhs2={res_g*0;res_f};
x2={x_1;[]};
res2=TEFEM.sol.direct_block(mat2,rhs2,x2,freeDOF);

res{1}=res2{1}+res1{1}.*dd_f.k(tri_grid.edge_center(:,1),tri_grid.edge_center(:,2));
res{2}=res2{2}./dd_f.k(tri_grid.elem_centres(:,1),tri_grid.elem_centres(:,2))+res1{2};
%% plotting
basis1.plot(res{1},tri_grid);
basis2.plot(res{2},tri_grid);