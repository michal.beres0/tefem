function [mat,rhs,x,freeDOF,basis1,basis2,tri_grid]=conv_diff(nx,delta,v)

%% Discretization size and problem setting
%nx=400; % discretization size x axis
ny=nx; % y axis
Lx=1;   % size of the domain x axis
Ly=1;   % y axis
smoothing_boundary = false; % deforms grid to be smoother around the boundary
quad_poly_degree=3;

%delta=1e-1;
%v=[1 0];

% (1-bottom,2-right,3-top,4-left))
% bound_dir={2, [0 1]; 3, [0 1]};
% bound_neu={1, [0 1]; 4, [0 1]};

bound_dir={1,[0 1]; 2, [0 1]; 3, [0 1]; 4, [0.4 .6]};
bound_neu={4, [0 0.4]; 4, [0.6 1]};

m_weak=@(v1,v2,data)1./data.delta.*TEFEM.op.dot(v1.val,v2.val);
b_weak=@(v1,v2,data)-TEFEM.op.trace(v2.grad).*v1.val;
c_weak=@(v1,v2,data)1./data.delta.*TEFEM.op.dot(data.v,v2.val).*v1.val;
f_weak=@(v1,data)-data.f.*v1.val;
u0_weak=@(v1,data)-data.u0.*TEFEM.op.dot(v1.val,data.n);

dd_f.delta=@(x,y)x*0+delta;
dd_f.v=@(x,y){x*0+v(1),x*0+v(2)};
dd_f.f=@(x,y)x*0+0;
dn_f.u0=@(x,y)double(y>0.5);
g=@(x,y)y*0;

basis1=TEFEM.fe.RT0();
basis2=TEFEM.fe.P0();

%% triangulation
[ tri_grid, bound_idx ] = TEFEM.grid.rect_mesh( Ly, Lx, ny, nx, smoothing_boundary);

%% domain integration
[qd] = TEFEM.grid.quad_point_triangles(tri_grid, quad_poly_degree);
[bf1] = basis1.val(tri_grid,qd);
[bf2] = basis2.val(tri_grid,qd);
for i=fieldnames(dd_f)'
    data.(i{1})=dd_f.(i{1})(qd.X{1},qd.X{2});
end

%% resolving boundary conditions
neu_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_neu);
dir_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_dir);

%% boundary integration
[qd_neu] = TEFEM.grid.quad_point_edges(tri_grid, neu_idx.idx_e, quad_poly_degree);
[bf1_neu] = basis1.val(tri_grid,qd_neu);
%[bf2_neu] = basis2.val(tri_grid,qd_neu);
for i=fieldnames(dn_f)'
    data.(i{1})=dn_f.(i{1})(qd_neu.X{1},qd_neu.X{2});
end
data.n=qd_neu.outer_normal;

%% boundary values
[x_1,freeDOF_1] = basis1.dir(tri_grid,dir_idx,g);

%% assembling matrices and vectors
[res_M] = TEFEM.assemble.bilinear(m_weak,bf1,bf1,data,qd);
[res_B] = TEFEM.assemble.bilinear(b_weak,bf2,bf1,data,qd);
[res_C] = TEFEM.assemble.bilinear(c_weak,bf2,bf1,data,qd);
[res_f] = TEFEM.assemble.linear(f_weak,bf2,data,qd);
[res_g] = TEFEM.assemble.linear(u0_weak,bf1_neu,data,qd_neu);

%% solution
freeDOF={freeDOF_1;[]};

mat={res_M,res_B';res_B+res_C,[]};
rhs={res_g;res_f*0};
x={x_1*0;[]};
end