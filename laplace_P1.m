%% Discretization size and problem setting
nx=400; % discretization size x axis
ny=nx; % y axis
Lx=1;   % size of the domain x axis
Ly=1;   % y axis
smoothing_boundary = false; % deforms grid to be smoother around the boundary
quad_poly_degree=10;

% (1-bottom,2-right,3-top,4-left))
bound_neu={1, [0 1]; 3, [0 1]};
bound_dir={2, [0 1]; 4, [0 1]};

a_weak=@(v1,v2,data)data.k.*TEFEM.op.dot(v1.grad,v2.grad);
f_weak=@(v1,data)data.f.*v1.val;
g_weak=@(v1,data)data.g.*v1.val;

dd_f.k=@(x,y)exp(2*sin(10*pi*x).*cos(10*pi*y));
dd_f.f=@(x,y)x*0+1;
dn_f.g=@(x,y)x*0+1;
u0=@(x,y)x;

basis1=TEFEM.fe.P1();

%% triangulation 
[ tri_grid, bound_idx ] = TEFEM.grid.rect_mesh( Ly, Lx, ny, nx, smoothing_boundary);

%% domain integration
[qd] = TEFEM.grid.quad_point_triangles(tri_grid, quad_poly_degree);
[bf1] = basis1.val(tri_grid,qd);
for i=fieldnames(dd_f)'
    data.(i{1})=dd_f.(i{1})(qd.X{1},qd.X{2});
end

%% resolving boundary conditions
neu_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_neu);
dir_idx=TEFEM.grid.select_boundary_edges_points(tri_grid,bound_idx,bound_dir);

%% boundary integration
[qd_neu] = TEFEM.grid.quad_point_edges(tri_grid, neu_idx.idx_e, quad_poly_degree);
[bf1_neu] = basis1.val(tri_grid,qd_neu);
for i=fieldnames(dn_f)'
    data.(i{1})=dn_f.(i{1})(qd_neu.X{1},qd_neu.X{2});
end

%% boundary values
[x,freeDOF] = basis1.dir(tri_grid,dir_idx,u0);

%% assembling matrices and vectors
[res_M] = TEFEM.assemble.bilinear(a_weak,bf1,bf1,data,qd);
[res_f] = TEFEM.assemble.linear(f_weak,bf1,data,qd);
[res_g] = TEFEM.assemble.linear(g_weak,bf1_neu,data,qd_neu);

%% solution
b=res_f+res_g-res_M*x;
x(freeDOF)=res_M(freeDOF,freeDOF)\b(freeDOF);

basis1.plot(x,tri_grid);
TEFEM.plot.plot_P1_pressure_flux(x,tri_grid,dd_f.k)